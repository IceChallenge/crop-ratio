﻿using System;
using System.Collections.Generic;

namespace CropRatioCodeChallenge
{
    public class CropRatio
    {
        private int totalWeight;
        private Dictionary<string, int> crops = new Dictionary<string, int>();

        public void Add(string name, int cropWeight)
        {
            int currentCropWeight;
            crops.TryGetValue(name, out currentCropWeight);

            if (currentCropWeight == 0)
                crops[name] = currentCropWeight;

            currentCropWeight += cropWeight;
            totalWeight++;
        }

        public double Proportion(string name)
        {
            return crops[name] / totalWeight;
        }

        static void Main(string[] args)
        {
            CropRatio cropRatio = new CropRatio();

            cropRatio.Add("Wheat", 4);
            cropRatio.Add("Wheat", 5);
            cropRatio.Add("Rice", 1);

            Console.WriteLine("Ratio of wheat: {0}", cropRatio.Proportion("Wheat"));
        }
    }
}
