﻿using System;
using System.Collections.Generic;

namespace Solution1
{
    public class CropRatio
    {
        private int totalWeight;
        private Dictionary<string, int> crops = new Dictionary<string, int>();

        public void Add(string name, int cropWeight)
        {
            int currentCropWeight = CropWeight(name);
            currentCropWeight += cropWeight;
            
            crops[name] = currentCropWeight;
            
            totalWeight += cropWeight;
        }

        public int CropWeight(string name)
        {
            int currentCropWeight = 0;
            crops.TryGetValue(name, out currentCropWeight);

            return currentCropWeight;
        }

        public double Proportion(string name)
        {
            double proportionValue = 0;

            if (crops.ContainsKey(name))
            {
                int currentCropWeight = CropWeight(name);
                proportionValue = (double)currentCropWeight / totalWeight;
            }

            return proportionValue;
        }

        static void Main(string[] args)
        {
            CropRatio cropRatio = new CropRatio();

            cropRatio.Add("Wheat", 4);
            cropRatio.Add("Wheat", 5);
            cropRatio.Add("Rice", 1);

            Console.WriteLine("Ratio of wheat: {0}", cropRatio.Proportion("Wheat"));
        }
    }
}
