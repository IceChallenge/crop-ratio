﻿using CropRatioNUnitTests.Extensions;
using NUnit.Framework;

namespace CropRatioNUnitTests
{
    internal abstract class BaseTests 
    {
        public object CropRatio { get; set; }

        [Test]
        public void TestExampleCase()
        {
            dynamic cropRatio = CropRatio.DeepCopyByExpressionTree();

            cropRatio.Add("Wheat", 4);
            cropRatio.Add("Wheat", 5);
            cropRatio.Add("Rice", 1);

            double value = cropRatio.Proportion("Wheat");

            Assert.IsTrue(value == 0.9);
        }

        [Test]
        public void TestSeveralCrops()
        {
            dynamic cropRatio = CropRatio.DeepCopyByExpressionTree();

            cropRatio.Add("Eggs", 2);
            cropRatio.Add("Wheat", 4);
            cropRatio.Add("Rice", 1);
            cropRatio.Add("Grapes", 3);

            double value = cropRatio.Proportion("Grapes");

            Assert.IsTrue(value == 0.3);
        }

        [Test]
        public void TestOnlyOneCrop()
        {
            dynamic cropRatio = CropRatio.DeepCopyByExpressionTree();

            cropRatio.Add("Wheat", 1);

            double value = cropRatio.Proportion("Wheat");

            Assert.IsTrue(value == 1);
        }

        [Test]
        public void TestZeroCrops()
        {
            dynamic cropRatio = CropRatio.DeepCopyByExpressionTree();

            double value = cropRatio.Proportion("Wheat");

            Assert.IsTrue(value == 0);
        }

    }
}
