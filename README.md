Visual Studio 2019 .NET Core 3.1 Console Application in C#

# Projects

    CropRatioCodeChallenge - Code Challenge for Developer

    CropRatioNUnitUnitTests - Test cases you can run against the Code Challenge
                            - Test cases you can run against Solution1 to see how each solution works

    Solution1 - One solution that solves each Test Case

# Instructions
    
    Open the "CropRatioCodeChallenge" Project
    Set your timer at 45:00 minutes and then start this exercise
    Run Unit Tests for "TestCasesForChallenge" to test your results

# Code Challenge

    The CropRatio object can be used to calculate what proportion of a farm's harvest is a specific crop.

    For example:

        CropRatio cropRatio = new CropRatio();
        cropRatio.Add("Wheat", 4);
        cropRatio.Add("Wheat", 5);
        cropRatio.Add("Rice", 1);

    Running cropRatio.Proportion("Wheat") should return 0.9.

    Fix the bugs.